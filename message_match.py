import json
from NaisterDatabase import Neo
from conf import NEO_PARAMS
neo = Neo(NEO_PARAMS)


def message_match(project_id, keyword_id):

    create_clause = f"kwd.keyword_id = {keyword_id} and kwd.project_id = {project_id}"

    # query to get kpis ok keyword: keyword_id

    q_kwd = f"""
                            MATCH (camp: Campaign)-[:HAS_ADGROUP]->(adg: Adgroup)-[:HAS_KEYWORD]->(kwd: Keyword)
                            WHERE {create_clause}
                            RETURN (kwd.keyword_name, kwd.naister_score, camp.campaign_name, adg.adgroup_name,
                                kwd.clics, kwd.impr, kwd.mean_cpc)
                            """

    # query to get Landing Page properties and scores related to keyword: keyword_id
    q_lp = f"""
                            MATCH (kwd: Keyword)-[r:HAS_LP]->(lp:LandingPage)
                            WHERE {create_clause}
                            RETURN (r.page_match_score, lp.url, lp.title, lp.desc, lp.comment, lp.h1, r.h1_score,
                                lp.h2, r.h2_score, lp.h3, r.h3_score, r.title_score, r.url_score, r.text_score,
                                lp.metas, r.metas_score)
                            """

    # query to get Adtext properties ans scores related to keyword: keyword_id
    q_adt = f"""
                            MATCH (kwd: Keyword)-[r:HAS_ADTEXT]->(adt:Adtext)
                            WHERE {create_clause}
                            RETURN (r.adtext_match_score, adt.adtext_h1, r.h1_score, adt.adtext_h2, r.h2_score,
                                adt.adtext_h3, r.h3_score, adt.adtext_description1, r.description1_score,
                                adt.adtext_description2, r.description2_score, adt.adtext_display_url,
                                r.url_score, adt.adtext_comments)
                            """

    dic_kwd = neo.graph.run(q_kwd).data()
    dic_lp = neo.graph.run(q_lp).data()
    dic_adt = neo.graph.run(q_adt).data()

    keyword_kpis = ["Naister Score", "Campaign", "Adgroup", "Impressions", "Clics", "CPC"]

    # getting keyword kpis
    kpis = [
        {
            "kpi": keyword_kpi,
            "value": dic_kwd[keyword_kpi],
        }
        for keyword_kpi in keyword_kpis
    ]

    # extraction headline scores and values  of Landing Page
    h1_values, h1_scores = dic_lp["h1"], dic_lp["h1_score"]
    h2_values, h2_scores = dic_lp["h2"], dic_lp["h2_score"]
    h3_values, h3_scores = dic_lp["h3"], dic_lp["h3_score"]
    h1_total_score = sum(h1_scores)/len(h1_scores)
    h2_total_score = sum(h2_scores)/len(h2_scores)
    h3_total_score = sum(h3_scores) / len(h3_scores)

    # getting Landing Page detailed scores
    page_detailed_scores = [
        {
            [
                {
                    "attribute": headline,
                    "score": h_total_score,
                    "values": [
                        {
                            "value": h_value,
                            "score": h_score,
                        }
                        for h_value, h_score in zip(h_values, h_scores)
                    ]
                }
                for headline, h_total_score, h_values, h_scores in
                zip(["h1", "h2", "h3"], [h1_total_score, h2_total_score, h3_total_score],
                    [h1_values, h2_values, h3_values], [h1_scores, h2_scores, h3_scores])
            ]
        },
        {
            "attribute": "URL",
            "value": dic_lp["url"]
            "score": dic_lp["url_score"],
        },
        {
            "attribute": "Text",
            "value": dic_lp["desc"]
            "score": dic_lp["text_score"],
        },
        {
            "attribute": "Metas",
            "value": dic_lp["metas"],
            "score": dic_lp["metas_score"],
        },
    ]

    # getting headlines scores and values of Adtext
    headline1_values, headline1_scores = dic_adt["adtext_h1"], dic_adt["h1_score"]
    headline2_values, headline2_scores = dic_adt["adtext_h2"], dic_adt["h2_score"]
    headline3_values, headline3_scores = dic_adt["adtext_h3"], dic_adt["h3_score"]
    headline1_total_score = sum(headline1_scores)/len(headline1_scores)
    headline2_total_score = sum(headline2_scores) / len(headline2_scores)
    headline3_total_score = sum(headline3_scores) / len(headline3_scores)

    # getting Adtext detailed scores
    adtext_detailed_scores = [
        {
            [
                {
                    "attribute": headline,
                    "score": headline_total_score,
                    "values": [
                        {
                            "value": h_value,
                            "score": h_score,
                        }
                        for h_value, h_score in zip(headline_values, headline_scores)
                    ]
                }
                for headline, headline_total_score, headline_values, headline_scores in
                zip(["h1", "h2", "h3"], [headline1_total_score, headline2_total_score, headline3_total_score],
                    [headline1_values, headline2_values, headline3_values],
                    [headline1_scores, headline2_scores, headline3_scores])
            ]
        },
        {
            [
                {
                    "attribute": description,
                    "score": description_score,
                    "values": [
                        {
                            "value": description_value,
                            "score": description_score,
                        }
                    ]
                }
                for description, description_score, description_value in
                zip(["description1", "description2"],
                    [dic_adt["description1_score"], dic_adt["description2_score"]],
                    [dic_adt["adtext_description1"], dic_adt["adtext_description2"]])
            ]
        },
        {
            "attribute": "preview URL",
            "score": dic_adt["url_score"],
            "values": [
                {
                    "value": dic_adt["adtext_display_url"],
                    "score": dic_adt["url_score"],
                }
            ]

        }
    ]

    # output
    data = {
        "keyword": {
            "keyword_name": dic_kwd["keyword_name"],
            "score": dic_kwd["naister_score"],
            "kpis": kpis,
        },
        "page": {
            "score": dic_lp["page_match_score"],
            "preview": {
                "url": dic_lp["url"],
                "title": dic_lp["title"],
                "desc": dic_lp["desc"],
                "comment": dic_lp["comment"],
            },
            "detailed_scores": page_detailed_scores,
        },
        "adtext": {
            "score": dic_adt["adtext_match_score"],
            "preview": {
                "adtext_h1": dic_adt["adtext_h1"],
                "adtext_h2": dic_adt["adtext_h2"],
                "adtext_h3": dic_adt["adtext_h3"],
                "adtext_description1": dic_adt["adtext_description1"],
                "adtext_description2": dic_adt["adtext_description2"],
                "display_url": dic_adt["display_url"],
                "comment": dic_adt["comment"],
            },
            "detailed_scores": adtext_detailed_scores,
        }
    }

    with open('message_match.json', 'w') as f:
        json.dump(data, f)
